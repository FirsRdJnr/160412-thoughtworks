# Thought Works UI Exercise #

### How do I get set up? ###

* Pull the repo
* NPM install
* Run `gulp`

### The Brief

I set up the app using npm and gulp - sass watchers and google font import, and used mustache.js as a template engine. There is some flight json, but this isn’t being manipulated in the dom, just called wholesale. There’s no build scripts so all the external assets used are just in index.html (which is messy!). 

I found designing this app really difficult as the brief raised more questions that I feel the scamp answered. Solely from a design perspective, I would never be handed a complete brief; I would hope that both design and development functions would be involved in getting to this stage, which ultimately helps in all aspects of building a product.

I switched to [materializecss] (http://materializecss.com/) half way though the project to facilitate work to be done to add  a manifest to be used with ‘add to homescreen’ in chrome to give a more native app experience. ([create launch experiences more comparable to native applications](https://developer.chrome.com/multidevice/android/installtohomescreen)).

Browser support can be a funny thing; it really comes down to what the customer uses. There have been fewer issues recently with modern browsers and mobile products, but it’s always best to check what the most common browser for the company is…it could be that this all has to be re-written to be compatible with ie8!

I don’t think I’ve designed or built anything that’s not responsive in one way or another for years now, but building something responsive is, again, all down to the customer using the app. Younger users are more accustomed to hidden menus but that might not be the answer to everything.
