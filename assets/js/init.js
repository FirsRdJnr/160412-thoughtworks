
// Touch listener
// -------------------------------------------------------------------------------

var listener = 'click';
    if ('ontouchstart' in window || window.navigator.maxTouchPoints) {
        listener = 'touchstart';
    }

// Add unique images to intro
// -------------------------------------------------------------------------------

function mainimage() {
    var images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg'];

    $('body').css({'background-image': 'url(assets/img/' + images[Math.floor(Math.random() * images.length)] + ')'});
}

// Load search form
// -------------------------------------------------------------------------------

function searchform() {

    $.getJSON('assets/js/data.json', function(data) {

        $.get('templates/searchform.mustache', function(template) {
            $('#searchform').html(Mustache.render(template, data));

            setInterval(function() {
                var randomDestination = data.flightdata[Math.floor(Math.random() * data.flightdata.length)]
                $('#searchform .destination :input').attr("placeholder", randomDestination.to + "?");
            }, 2000);

            datepicker();
            $('select').not('.disabled').material_select();

            priceSlider();
            findFlights();

        });
    });
}

// Get flight data action
// -------------------------------------------------------------------------------

function findFlights() {
    $('.find-flights .btn').on(listener, function() {
        $(this).text('Update search');

        $('.mobile-btn').removeClass('hide');

        $('.inital-search').removeClass('valign-wrapper').addClass('row');
        $('.inital-search section').removeClass('valign open').addClass('col s12 m4 l3 sidebar');

        flightdata();
    });
}


// Get flight data
// -------------------------------------------------------------------------------

function flightdata() {

    // Data stolen from this guy! https://github.com/prateekagr98/Dummy-Flight-Search-Website/blob/master/json/flights.json
    $.getJSON('assets/js/data.json', function(data) {

        $.get('templates/flightdata.mustache', function(template) {
            $('#flightdata').html(Mustache.render(template, data));
        });
    });
}


// Set price slider
// -------------------------------------------------------------------------------

function priceSlider() {
    var slider = document.getElementById('range-input');
    noUiSlider.create(slider, {
        start: [50, 150],
        connect: true,
        step: 1,
        range: {
            'min': 0,
            'max': 200
        },
        format: wNumb({
            decimals: 0
        })
    });
}


// Datepicker functions
// -------------------------------------------------------------------------------

function datepicker() {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15
    });
}

// open side menu on mobile
// -------------------------------------------------------------------------------

function openMenu() {
    $('.mobile-btn').on(listener, function() {
        $(this).children('.fa').toggleClass('fa-arrow-left');
        $('.sidebar').toggleClass('open');
    });
}




// Load functions
// -------------------------------------------------------------------------------

$(document).ready(function(){
    openMenu();
    searchform();
    mainimage();

});

$(window).resize(function() {
});

$(window).scroll(function(){
});



