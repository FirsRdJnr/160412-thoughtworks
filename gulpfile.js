'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var googleWebFonts = require('gulp-google-webfonts');


// Build sass
// -------------------------------------------------------------------------------
gulp.task('sass', function () {
    return gulp.src('./assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/'));
        .pipe(livereload());
});


// Import Google fonts and compile sass for
// -------------------------------------------------------------------------------
var options = {
    fontsDir: 'fonts/',
    cssDir: 'sass/',
    cssFilename: '_googlefonts.scss'
};

gulp.task('fonts', function () {
    return gulp.src('./fonts.list')
        .pipe(googleWebFonts(options))
        .pipe(gulp.dest('assets/'));
    });


// Watch for updates
// -------------------------------------------------------------------------------
gulp.task('watch', function () {
    .pipe(livereload());
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
});


// Default: Build all the things!
// -------------------------------------------------------------------------------
gulp.task('default', function() {
    gulp.start('sass', 'fonts');
});
